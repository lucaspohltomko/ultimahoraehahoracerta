<?php
include_once '../models/dbconfig.php';
include_once '../models/class.user.php';

if($user->is_loggedin())
{
    $user_id = $_SESSION['user_session'];
    $stmt = $DB_con->prepare("SELECT * FROM users WHERE user_id=:user_id");
    $stmt->execute(array(":user_id"=>$user_id));
    $userRow=$stmt->fetch(PDO::FETCH_ASSOC);
}
else{
    $userRow = array("user_name");
    $userRow['user_name'] = "nao logado"; 
}



?>
<div class="dropdown user-dropdown col-md-6 col-lg-4 text-center text-md-right"><a class="btn btn-stripped dropdown-toggle" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <img src="../images/profile.jpg" alt="profile photo" class="circle float-left profile-photo" width="50" height="auto">
        <div class="username mt-1">
            <h4 class="mb-1"><?php print($userRow['user_name']); ?> </h4>
            <h6 class="text-muted">
            </h6>
        </div>
    </a>
    <div class="dropdown-menu dropdown-menu-right" style="margin-right: 1.5rem;" aria-labelledby="dropdownMenuLink">

        <?php
        require_once '../models/dbconfig.php';


        if($user->is_loggedin()=="" ){
            ?>
        <a class="dropdown-item" href="login.php"><em class="fa fa-user-circle mr-1"></em> login</a>
        <?php
        }
        ?>
        <a class="dropdown-item" href="edit_usuario.php?edit_id=<?=$_SESSION['user_session']?>"><em class="fa fa-sliders mr-1"></em>Editar Dados</a>
        <a class="dropdown-item" href="logout.php"><em class="fa fa-power-off mr-1"></em> Logout</a></div>
</div>