<?php
class USER
{
    private $db;
 
    function __construct($DB_con)
    {
      $this->db = $DB_con;
    }
 
    public function register($fname,$lname,$uname,$umail,$upass)
    {
       try
       {
           $new_password = password_hash($upass, PASSWORD_DEFAULT);
   
           $stmt = $this->db->prepare("INSERT INTO users(user_name,user_email,user_pass) 
                                                       VALUES(:uname, :umail, :upass)");
              
           $stmt->bindparam(":uname", $uname);
           $stmt->bindparam(":umail", $umail);
           $stmt->bindparam(":upass", $new_password);            
           $stmt->execute(); 
   
           return $stmt; 
       }
       catch(PDOException $e)
       {
           echo $e->getMessage();
       }    
    }
 
    public function login($uname,$umail,$upass)
    {
       try
       {
          $stmt = $this->db->prepare("SELECT * FROM users WHERE user_name=:uname OR user_email=:umail LIMIT 1");
          $stmt->execute(array(':uname'=>$uname, ':umail'=>$umail));
          $userRow=$stmt->fetch(PDO::FETCH_ASSOC);
          if($stmt->rowCount() > 0)
          {
             if(password_verify($upass, $userRow['user_pass']))
             {
                $_SESSION['user_session'] = $userRow['user_id'];
                if($userRow['id_tip'] == 1){
                    $_SESSION['admin'] = "ativa";
                }
                else{
                    $_SESSION['admin'] = "desativa";                    
                }
                return true;
             }
             else
             {
                return false;
             }
          }
       }
       catch(PDOException $e)
       {
           echo $e->getMessage();
       }
   }

   public function logout()
   {
        session_destroy();
        unset($_SESSION['user_session']);
    
        
   }

   public function update_user($novo_nome,$novo_email,$novo_senha,$user_id)
   {
    try
    {
        
       $stmt = $this->db->prepare("UPDATE users SET user_name=:novo_nome, user_email=:novo_email, user_pass=:novo_senha WHERE user_id = :user_id");
       
       //$senhaEncr = password_hash($nova_senha, PASSWORD_DEFAULT);

       $stmt->bindparam(":novo_nome", $novo_nome);
       $stmt->bindparam(":novo_email", $novo_email);
       $stmt->bindparam(":novo_senha", password_hash($novo_senha, PASSWORD_DEFAULT));
       $stmt->bindparam(":user_id", $user_id);    
       $stmt->execute(); 

       return $stmt;
    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    }
   }
   public function delete_user($user_id)
   {
    try
    {
       $stmt = $this->db->prepare("DELETE FROM users WHERE user_id = :user_id");
       
       $stmt->bindparam(":user_id", $user_id);    
       $stmt->execute(); 

       return $stmt;
       
    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    }
   }

   public function is_loggedin()
   {
      if(isset($_SESSION['user_session']))
      {
         return true;
      }
   }
 
   public function redirect($url)
   {
       header("Location: $url");
   }
 
   
public function registrar_egresso($id, $nome_completo, $cpf, $cod_matricula, $ano_ingresso, $curso, $cel, $localizacao, $endereco, $nascimento, $sexo, $deficiencia, $conc_curso, $campus, $nota_ifc, $projeto, $area_formacao, $cursos_extra, $palestra, $atuacao, $trabalho, $motivo_trabalho, $salario, $texto)
    {
       try
       {
           $stmt2 = $this->db->prepare("INSERT INTO egresso(id, nome_completo, cpf, cod_matricula, ano_ingresso,curso, cel, localizacao, endereco, nascimento, sexo, deficiencia, conc_curso, campus, nota_ifc, projeto, area_formacao, cursos_extra, palestra, atuacao, trabalho, motivo_trabalho, salario, texto) 
           VALUES(:id,:nome_completo, :cpf, :cod_matricula, :ano_ingresso, :curso, :cel, :localizacao, :endereco, :nascimento, :sexo, :deficiencia, :conc_curso, :campus, :nota_ifc, :projeto, :area_formacao, :cursos_extra, :palestra, :atuacao, :trabalho, :motivo_trabalho, :salario, :texto )");
           $stmt2->bindparam(":id", $id);

           $stmt2->bindparam(":nome_completo", $nome_completo);
           $stmt2->bindparam(":cpf", $cpf);
           $stmt2->bindparam(":cod_matricula", $cod_matricula);       
           $stmt2->bindparam(":ano_ingresso", $ano_ingresso);       
           $stmt2->bindparam(":curso", $curso);
           $stmt2->bindparam(":cel", $cel);
           $stmt2->bindparam(":localizacao", $localizacao);
           $stmt2->bindparam(":endereco", $endereco);
           $stmt2->bindparam(":nascimento", $nascimento);
           $stmt2->bindparam(":sexo", $sexo);
           $stmt2->bindparam(":deficiencia", $deficiencia);
           $stmt2->bindparam(":conc_curso", $conc_curso);
           $stmt2->bindparam(":campus", $campus); 
           $stmt2->bindparam(":nota_ifc", $nota_ifc);
           $stmt2->bindparam(":projeto", $projeto);
           $stmt2->bindparam(":area_formacao", $area_formacao);
           $stmt2->bindparam(":cursos_extra", $cursos_extra);
           $stmt2->bindparam(":palestra", $palestra);
           $stmt2->bindparam(":atuacao", $atuacao);
           $stmt2->bindparam(":trabalho", $trabalho);
           $stmt2->bindparam(":motivo_trabalho", $motivo_trabalho);
           $stmt2->bindparam(":salario", $salario);
           $stmt2->bindparam(":texto", $texto);




           $stmt2->execute(); 
   
           return $stmt2; 
       }
       catch(PDOException $e)
       {
           echo $e->getMessage();
       }    
    }

    public function mostra_egressos()
    {
        $stmt = $this->db->prepare('SELECT * FROM egresso');
		$stmt->execute();
        $egressos = $stmt->fetchALL(PDO::FETCH_ASSOC);
        return $egressos;
    }
    
    public function mostra_users()
    {
        $stmt = $this->db->prepare('SELECT * FROM users');
		$stmt->execute();
        $usuarios = $stmt->fetchALL(PDO::FETCH_ASSOC);
        return $usuarios;
    }

    public function procura_egresso($nome_pesquisa){

		$nome_pesquisa = htmlspecialchars($nome_pesquisa);

		$procura = "SELECT nome_completo FROM egresso WHERE nome_completo LIKE '%".$nome_pesquisa."%'";
		$resultado_parcial = $this->conn->query($procura);
		$resultado_pesquisa = $resultado_parcial->fetch(PDO::FETCH_ASSOC);
		
		return $resultado_pesquisa;

	}
    
}
?>