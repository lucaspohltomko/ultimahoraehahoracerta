-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 27-Nov-2018 às 07:28
-- Versão do servidor: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_login`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `egresso`
--

CREATE TABLE `egresso` (
  `id` int(11) NOT NULL,
  `nome_completo` varchar(120) NOT NULL,
  `cpf` int(11) NOT NULL,
  `cod_matricula` int(40) NOT NULL,
  `ano_ingresso` int(4) NOT NULL,
  `curso` varchar(60) NOT NULL,
  `cel` int(20) NOT NULL,
  `localizacao` varchar(40) NOT NULL,
  `endereco` varchar(120) NOT NULL,
  `nascimento` date NOT NULL,
  `sexo` varchar(40) NOT NULL,
  `deficiencia` varchar(40) NOT NULL,
  `conc_curso` varchar(40) NOT NULL,
  `campus` varchar(40) NOT NULL,
  `nota_ifc` varchar(40) NOT NULL,
  `projeto` varchar(40) NOT NULL,
  `area_formacao` varchar(40) NOT NULL,
  `cursos_extra` varchar(80) NOT NULL,
  `palestra` varchar(80) NOT NULL,
  `atuacao` varchar(80) NOT NULL,
  `trabalho` varchar(40) NOT NULL,
  `motivo_trabalho` varchar(40) NOT NULL,
  `salario` varchar(40) NOT NULL,
  `texto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `egresso`
--

INSERT INTO `egresso` (`id`, `nome_completo`, `cpf`, `cod_matricula`, `ano_ingresso`, `curso`, `cel`, `localizacao`, `endereco`, `nascimento`, `sexo`, `deficiencia`, `conc_curso`, `campus`, `nota_ifc`, `projeto`, `area_formacao`, `cursos_extra`, `palestra`, `atuacao`, `trabalho`, `motivo_trabalho`, `salario`, `texto`) VALUES
(10, 'Pontianus Heinz', 42943895, -1912710627, 2018, 'kurxkg', 293, 'Saskatchewan', '42-28 Milson Road, Coventry, Armagh BA14 6NQ', '1983-11-17', 'Female', 'zj', 'ikdjmaimw', 'araquari', 'mbom', 'd', 'Customer Support', 'xtfcsrlzct', 'ibxbc', 'gbdhcuko', 'hz', 'g', '10000', 'g'),
(16, 'Amrei ozart', 86411502, -1912710626, 2013, 'lakwk', 159630492, 'Nunavut', '14-49 Annette Road, Plymouth, Sutherland NG9 8PY', '1978-12-06', 'Male', 'ps', 'wrglrlvav', 'araquari', 'mbom', 'u', 'Operations', 'c', 'dxejfd', 'jv', 'vyytnct', 'f', '10000', 'm'),
(30, 'Aurion  Günther', 19198840, -1912710625, 2016, 'ggwgskeq', 324898873, 'Nova Scotia', '3-9 Crestfield Street, Burnley, Angus PR4 7QJ', '1970-04-08', 'Male', 'mn', 'surxfctel', 'sombrio', 'otimo', 'r', 'Consulting', 'aifdawnzn', 'jk', 'tsclz', 'nhdba', 'p', '10000', 'q'),
(41, 'Volbrecht Angst', 90018026, -1912710624, 2011, 'up', 7465, 'Nova Scotia', '14-37 Ariel Way, Duns, Wiltshire HG88 4FE', '2008-12-25', 'Female', 'cnt', 'kfnemtmwx', 'sombrio', 'bom', 'a', 'Consulting', 'gmlrrhol', 'tpaq', 'ik', 'bv', 'a', '10000', 'u'),
(7, 'Hannfried Blume', 93997347, -1483213897, 2015, 'bkbeo', 1499041676, 'New Brunswick', '55-57 Woodcroft Road, Folkestone, Fife BB1 6SY', '2004-03-12', 'Male', 'mh', 'eshhgihqk', 'araquari', 'regular', 'i', 'Operations', 'gsvabuyhu', 'fpnbcdgm', 'cdhmhrgsdh', 'vhaqcftj', 'g', '10000', 'q'),
(14, 'Falina Lohmann', 19412832, -1483213896, 2010, 'xblqhuz', 5, 'Nunavut', '2-7 Wandon Road, Blackburn, Warwickshire SY2 9DW', '1970-01-01', 'Male', 'ag', 'amqxythgj', 'sombrio', 'otimo', 'm', 'Information Technolo', 'c', 'liekcdofiz', 'kareieamfj', 'damwikr', 'y', '10000', 'd'),
(22, 'Barthel  Mauch', 7442284, -1483213895, 2012, 'omiq', 1051894402, 'Quebec', '1-7 Clapham High Street, Ballater, Lancashire MK29 5YS', '1977-05-09', 'Female', 'qby', 'vumjczhps', 'concórdia', 'otimo', 'e', 'Sales', 'zrbjcsykxz', 'oi', 'ldpsmd', 'u', 't', '10000', 't'),
(36, 'Dietlana Auerbach', 38098074, -1483213894, 2013, 'ijqbbhu', 9453, 'Alberta', '5-6 Blackwall Lane, Telford, Lincolnshire LA20 4HP', '1998-03-21', 'Não Especificar', 'wt', 'xdiprppbv', 'araquari', 'otimo', 't', 'Research and Develop', 'behx', 'qjlk', 'qfpv', 'mrc', 'a', '10000', 'c'),
(47, 'Justus Laufer', 48691584, -1483213893, 2015, 'whzzs', 453352138, 'Nunavut', '78 Broadlands Road, New Malden, Bedfordshire CV8 1QB', '1998-09-03', 'Male', 'hyv', 'bcuzovjgt', 'araquari', 'bom', 'x', 'Executive', 'hqbssntgm', 'apkpmypoae', 'zho', 'himedjx', 'z', '10000', 'm'),
(4, 'Hemma Horkheimer', 14273034, -1127246428, 2010, 'whximakx', 212635637, 'Saskatchewan', '1A Wood Lane, Bicester, Inverness-shire FK5 4NJ', '2009-09-30', 'Female', 'jmu', 'mlshyzqls', 'araquari', 'bom', 'f', 'Facilities', 'brdohcm', 'ktfetkddb', 'agnlbk', 'mzext', 'f', '10000', 'v'),
(24, 'Wilbrecht Ebel', 82183373, -1127246427, 2010, 'heaflawl', 1692666233, 'New Brunswick', '4 Aberconway Road, Abbots Langley, Cromartyshire DG56 4VV', '1999-06-25', 'Female', 'rm', 'caeficldh', 'concórdia', 'otimo', 'a', 'Marketing', 'phebqnop', 'ilqcw', 'ho', 'ymmmxelk', 'm', '10000', 'm'),
(39, 'Janeric Thalberg', 42927526, -1127246426, 2013, 'ea', 1466295982, 'British Columbia', '35 Ensign Street, Rotherham, West Lothian IP8 7QY', '1975-11-13', 'Female', 'yd', 'wwiuasius', 'sombrio', 'regular', 'u', 'Research and Develop', 'vjhxmx', 'xqrjaw', 'u', 'rqfdb', 'a', '10000', 'u'),
(20, 'Korth Baur', 80729054, -697749697, 2016, 'yhb', 645491819, 'Quebec', '1 A-D Coppice Way, Aberaeron, Cheshire KT82 9YW', '1970-03-24', 'Female', 'lgj', 'znowiuebc', 'araquari', 'fraco', 'g', 'Information Technolo', 'tekrocvxuw', 'g', 'hb', 'kakebm', 's', '10000', 'y'),
(38, 'Diepold Arnold', 19353589, -697749696, 2018, 'bijn', 16588874, 'Alberta', '24-59 Bedford Road, Romney Marsh, Merseyside TA8 6RF', '1990-09-01', 'Male', 'cb', 'ksmnstnnv', 'concórdia', 'bom', 'd', 'Executive', 'lhgi', 'hjkajxzgrz', 'sktix', 'xxriy', 'l', '10000', 's'),
(15, 'Lovis Jens', 69934292, -23652815, 2010, 'wdlqr', 610245999, 'Yukon Territory', '4 Fortis Green Road, Norwich, County Durham N8 3EF', '2004-02-27', 'Female', 'zqz', 'gbfdekkzl', 'sombrio', 'otimo', 'j', 'Consulting', 'tuyn', 'ctqlcu', 'manlz', 'exc', 'n', '10000', 'h'),
(23, 'Krispin Heckler', 39903514, -23652814, 2016, 'rqsds', 1500377595, 'Quebec', '32-28 Charlton Road, Colchester, Berwickshire LL96 9JD', '1995-04-14', 'Female', 'ga', 'japhjmobb', 'araquari', 'bom', 'z', 'Operations', 'wmblwlssi', 'w', 'e', 'x', 'z', '10000', 'w'),
(31, 'Friedwart osenblatt', 25789537, -23652813, 2015, 'gvherelp', 1596070849, 'Alberta', '2-6 Overbury Road, Milton Keynes, Northamptonshire NP8 2XP', '1998-02-09', 'Male', 'jxe', 'vtnnrpbiv', 'araquari', 'bom', 'u', 'Information Technolo', 'ajihmpht', 'kgwqptlq', 'grwnakrbon', 'lqak', 'e', '10000', 'o'),
(33, 'Abeline  Kramer', 40961522, -23652812, 2016, 'ohzbmsiv', 1028149763, 'Nunavut', '6 Marlborough Road, Burford, Sutherland HU4 7DV', '2000-05-04', 'Female', 'jgn', 'yecuhwiex', 'sombrio', 'regular', 'd', 'Executive', 'jhsvxjtz', 'alfpxzrlsp', 'bedeujaevx', 'zxzzyyh', 'a', '10000', 'b'),
(43, 'Avenell olken', 76292304, -23652811, 2013, 'woaseop', 2018542187, 'Manitoba', '34-46 Crown Place, Henley-on-Thames, Banffshire TW58 9DA', '1984-01-16', 'Female', 'kcu', 'bvbqbqmdx', 'sombrio', 'otimo', 'l', 'Sales', 'd', 'frqgbcgxe', 'enxbzldgd', 'evkmwg', 'n', '10000', 'y'),
(46, 'Yente Fischnaller', 76219364, -23652810, 2018, 'zswzmi', 348348009, 'Nunavut', '1-9 Hyde Park Place, St Helens, Cumberland BH2 8DG', '1997-04-15', 'Não Especificar', 'coh', 'iihtxckce', 'concórdia', 'fraco', 'b', 'Facilities', 'npa', 'heak', 'fgaq', 'efxx', 'b', '10000', 'h'),
(18, 'Conny Dippel', 70906382, 405843916, 2013, 'nhriv', 7517, 'Prince Edward Island', '13-26 Eden Grove, Stone, Caithness SE14 9XV', '1972-07-30', 'Male', 'cfe', 'jzflwmmgy', 'concórdia', 'mbom', 'd', 'Research and Develop', 'zdkavxwevy', 'bejbypiui', 'dpfxcqkx', 'kxdy', 'y', '10000', 'm'),
(26, 'Rüder Hermann', 24007613, 405843917, 2016, 'nedlbeon', 7417, 'Ontario', '5 Forest Road, Martock, Angus BN1 6UZ', '1993-09-20', 'Male', 'nb', 'qglgijgvp', 'sombrio', 'otimo', 'x', 'Information Technolo', 'de', 'mfgaq', 'ehucuwx', 'glpqdfg', 'n', '10000', 'b'),
(32, 'Cölestin Treviranus', 93281662, 405843918, 2015, 'dy', 226543879, 'Alberta', '5A Beck Close, Lower Edmonton, Greater Manchester CM3 6JS', '1994-11-26', 'Female', 'efr', 'icjakaber', 'sombrio', 'fraco', 't', 'Executive', 'gxokjkodw', 'fqhshj', 'emx', 'lcmopt', 'e', '10000', 'w'),
(40, 'Adriane  chs', 91509238, 405843919, 2017, 'myea', 1541881768, 'Nova Scotia', '42-36 Abercorn Crescent, Blackburn, Peeblesshire SG1 6UP', '1996-01-24', 'Male', 'ae', 'zclbgbsvr', 'araquari', 'otimo', 'q', 'Research and Develop', 'mk', 'gz', 'cwuo', 'nqehkkd', 'h', '10000', 'm'),
(44, 'Annalena Lichtenberg', 6983240, 405843920, 2012, 'nopbqo', 533092023, 'Yukon Territory', '8 A-D Parkgate Road, Rayleigh, Selkirkshire TQ75 6BX', '1970-09-02', 'Male', 'koa', 'eyfchzscm', 'araquari', 'otimo', 'p', 'Manufacturing', 'z', 'cpuvp', 'nyxotxl', 't', 'b', '10000', 'h'),
(2, 'Heiderose Klotz', 84431034, 766503076, 2017, 'xn', 1267385752, 'Prince Edward Island', '6 Prescot Street, Coventry, Argyll SO90 2EU', '1975-10-12', 'Male', 'rf', 'okdikbzgl', 'araquari', 'bom', 'z', 'Executive', 'dmortanrf', 'ehf', 'dmwvyp', 'flmvbb', 'e', '10000', 'z'),
(6, 'Orthey Vogt', 44205099, 766503077, 2010, 'fcj', 491059674, 'Nova Scotia', '13-49 Childebert Road, Liverpool, Lanarkshire LE7 6VF', '1987-06-10', 'Male', 'st', 'cecyfsadu', 'sombrio', 'bom', 'g', 'Information Technolo', 'gmgcjddaui', 'cfbkefn', 'ygyfwud', 'yqya', 'n', '10000', 'a'),
(17, 'Catrin Gutzeit', 84468814, 766503078, 2010, 'jwp', 511, 'Nunavut', '86 A-C Rowse Close, Sheffield, Suffolk N2 2UJ', '1986-12-20', 'Male', 'pu', 'ketwlpyhc', 'araquari', 'mbom', 'm', 'Finance', 'ccaixlyym', 'm', 'kihfdnuwq', 'nnhpdvh', 'n', '10000', 'c'),
(21, 'Lenchen Schwarz', 59152277, 766503079, 2010, 'pufdqahy', 2125522354, 'Quebec', '7 Cadogan Place, Dalry, Huntingdonshire HU8 4XQ', '1996-07-13', 'Female', 'lx', 'udjckohvo', 'concórdia', 'otimo', 't', 'Research and Develop', 'g', 'vyvrfd', 'x', 'fngrwrjh', 'z', '10000', 'l'),
(34, 'Balte  Joseph', 52161209, 766503080, 2010, 'mqma', 548, 'Quebec', '1-7 St. John Street, Stockport, Selkirkshire HG6 3WW', '1979-11-27', 'Male', 'eb', 'dimzscuxg', 'araquari', 'otimo', 'w', 'Research and Develop', 'xjoiqrfizq', 'ouyorygezz', 'ivmoedqli', 'qb', 'e', '10000', 'c'),
(37, 'Mirinda Bartels', 10903294, 766503081, 2016, 'ijbwrn', 1532321546, 'Nova Scotia', '2-7 White Church Lane, Rotherham, Worcestershire NR43 7NR', '1975-12-30', 'Male', 'sp', 'akqjwkoss', 'sombrio', 'bom', 'r', 'Research and Develop', 'fboqpiye', 'wtbzn', 'zokrlro', 'f', 'p', '10000', 'y'),
(48, 'Ottfried Abel', 60359268, 766503082, 2016, 'svg', 43, 'Nova Scotia', '4 A-C Newington Green Road, Aberdovey, Oxfordshire W53 9BU', '1978-09-20', 'Female', 'ha', 'wipuegflr', 'sombrio', 'mbom', 'd', 'Facilities', 'yeplearvnd', 'hx', 'txfs', 'avfkbc', 'g', '10000', 'd'),
(5, 'Helwig osenbaum', 26533552, 1195999807, 2012, 'z', 1944906643, 'Quebec', '4-9 Abbey Street, Llanbrynmair, Peeblesshire KA4 9ZR', '2005-07-17', 'Male', 'yy', 'zfjggsifs', 'concórdia', 'otimo', 'f', 'Consulting', 'tsif', 'kbufbwob', 'nbczqrwgyn', 'rbomzrtz', 'y', '10000', 'n'),
(11, 'Berengar Heitsch', 38936855, 1195999808, 2016, 'hmqvb', 960343754, 'Newfoundland and Labrador', '3-9 Camberwell Station Road, Saltburn, Dunbartonshire BR66 4EU', '2015-06-18', 'Male', 'le', 'waphomevz', 'araquari', 'regular', 'u', 'Marketing', 'r', 'tegabllcfw', 'jkahzibwb', 'umil', 'x', '10000', 'd'),
(19, 'Armgard Kopf', 25518242, 1195999809, 2011, 'mg', 2093438949, 'Nova Scotia', '83 Celtic Farm Way, Southend-on-Sea, Northumberland EN17 2QN', '2008-03-29', 'Male', 'uu', 'ntomgkgfn', 'concórdia', 'regular', 'o', 'Sales', 'tnqwpaeww', 'sr', 'c', 'sckfcst', 'e', '10000', 'x'),
(29, 'Lisa-Maria Marschner', 43913875, 1195999810, 2018, 'co', 3, 'Prince Edward Island', '15-56 Sandwell Crescent, Southport, Dumfriesshire LE2 2WW', '1971-01-12', 'Female', 'lf', 'jciharuqe', 'sombrio', 'otimo', 'a', 'Consulting', 'ooccp', 'ytcmaxkb', 'zncbiby', 's', 'c', '10000', 'd'),
(35, 'Chlothar Franz', 25826610, 1195999811, 2017, 'mr', 5853, 'Prince Edward Island', '2-7 Stable Street, Wingate, Dumfriesshire DE89 5AZ', '2011-08-24', 'Female', 'xpb', 'jbqykwers', 'araquari', 'fraco', 'z', 'Operations', 'i', 'lypfaqwe', 'duy', 'zz', 'h', '10000', 'n'),
(45, 'Haita aulig', 83037014, 1195999812, 2016, 'n', 2137923425, 'Nova Scotia', '52-27 Canada Place, Wolverhampton, County Durham TQ52 5SH', '1970-05-22', 'Female', 'uo', 'yjqdevduw', 'araquari', 'mbom', 'c', 'Customer Support', 'lyzfbiyww', 'gdcllztm', 'cbob', 'ktycm', 'h', '10000', 'e'),
(50, 'Friederun Appel', 8699597, 1195999813, 2012, 'vctilf', 2106009789, 'New Brunswick', '5 Penton Street, Augher, Dumfriesshire SW5 2TA', '1997-02-22', 'Female', 'cu', 'blrfzmqwo', 'araquari', 'bom', 'u', 'Manufacturing', 'kzjmrfr', 'olqjbqzvdg', 'qn', 'vmrjuhrn', 'y', '10000', 'f'),
(8, 'Xenja Kirchner', 27179568, 1530041964, 2014, 'gjhhu', 650420614, 'Nunavut', '26D South Molton Street, Hindhead, Banffshire TQ2 3DY', '2009-09-13', 'Male', 'wn', 'qxkghuwrs', 'araquari', 'regular', 'w', 'Operations', 'uwvx', 'd', 'jjmb', 'zmjlih', 'j', '10000', 'v'),
(12, 'Clemendina Tischler', 42642573, 1530041965, 2011, 'y', 1663525656, 'Yukon Territory', '8 A-B Ashburnham Road, Preston, Berkshire B6 2FF', '1970-01-05', 'Female', 'qk', 'elvadwtkv', 'sombrio', 'bom', 'u', 'Marketing', 'ikbvie', 'wbxgtr', 'z', 'tyfrfjp', 'h', '10000', 'z'),
(25, 'Dieter ietzsche', 57865381, 1530041966, 2014, 'axyoke', 1267445198, 'New Brunswick', '4-7 Blackhorse Lane, Dudley, Oxfordshire TW1 8BU', '1970-11-09', 'Male', 'lge', 'manrzknrx', 'araquari', 'mbom', 'c', 'Facilities', 'x', 'zhnbyfia', 'kjxbmou', 'jqbt', 'f', '10000', 'g'),
(28, 'Fredegar chultz', 34448279, 1530041967, 2012, 'sveci', 1292597871, 'Manitoba', '55-26 Broughton Road, Ballymoney, Orkney SW71 7DS', '1970-03-29', 'Female', 'zlh', 'vfvmebecu', 'sombrio', 'otimo', 'r', 'Consulting', 'csjsbur', 'awznvahw', 'jtkaqawgkk', 'yc', 'x', '10000', 'y'),
(49, 'Almute Josef', 27987329, 1530041968, 2018, 'jxxz', 1049018051, 'Quebec', '12-49 Cleves Road, Banbridge, Roxburghshire WN33 8YX', '1999-09-29', 'Female', 'fec', 'yemtuwdte', 'concórdia', 'otimo', 'x', 'Executive', 'tl', 'fh', 'lzukfckn', 'gbn', 'k', '10000', 'j'),
(1, 'Bertrada Fährmann', 94630842, 1959538689, 2015, 'khy', 25718507, 'Prince Edward Island', '14-37 Bridgeman Street, Dereham, Caithness WA75 4XD', '1971-03-09', 'Male', 'lbx', 'yvftxacbb', 'concórdia', 'bom', 't', 'Executive', 'kbcwqbd', 'btd', 'gf', 'xqyqs', 'a', '10000', 'u'),
(9, 'Selinda Keller', 19149567, 1959538690, 2015, 'pc', 532, 'British Columbia', '5 A-D Torbay Street, Dalry, Northamptonshire G52 4AZ', '1970-03-05', 'Male', 'zqz', 'gvfeotlng', 'concórdia', 'otimo', 'u', 'Consulting', 'aoncrbiczt', 'y', 'xxqgpmmv', 'uf', 'b', '10000', 'w'),
(13, 'Adine  Wulf', 10030240, 1959538691, 2017, 'c', 2104736000, 'Prince Edward Island', '4 Denman Street, Bolton, Peeblesshire MK71 4EH', '1987-10-06', 'Male', 'gc', 'pacatmpin', 'araquari', 'fraco', 'f', 'Research and Develop', 'v', 'axidvv', 'zvyuuf', 'oofl', 'd', '10000', 'u'),
(27, 'California Zabel', 96147348, 1959538692, 2016, 'hdeidy', 1041544547, 'Manitoba', '8 A-E Sandland Street, Ambleside, County Durham TW4 7WA', '2007-10-22', 'Male', 'ch', 'ewmrqyrxz', 'concórdia', 'mbom', 'n', 'Sales', 'gd', 'lhbzr', 'jhmzccl', 'ehfmvzw', 'a', '10000', 'n'),
(42, 'Haiko erl', 73555605, 1959538693, 2012, 'herma', 2096449567, 'Yukon Territory', '7 Whitecross Road, Blakeney, Suffolk SN8 7NX', '1990-11-01', 'Não Especificar', 'ij', 'hctzqpcxq', 'concórdia', 'otimo', 'g', 'Human Resources', 'o', 'yzwaglak', 'ajw', 'lxk', 'h', '10000', 'u'),
(3, 'Joko Adler', 95876234, 2147483647, 2013, 'zakfecsg', 1686768292, 'Alberta', '5 Bedford Way, Middlesbrough, Selkirkshire W45 4VG', '1983-12-29', 'Male', 'gh', 'joumnadjv', 'araquari', 'otimo', 'u', 'Information Technolo', 'hdnwwellv', 'axhbddcy', 'v', 'w', 'y', '10000', 's');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(60) NOT NULL,
  `user_pass` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_email`, `user_pass`) VALUES
(1, 'Abreu2022', 'DeeannaAbreu59@example.com', '6P4674GS4O14Q849C9495BXI4V2Z1672S6OE7IG4XIEMCD6BPH61I48NR1P9U2LTN2Q9IY3'),
(2, 'Cathey1980', 'ZonaAmaral638@nowhere.com', 'RH80RK7KXS601'),
(3, 'Aleta1980', 'Addison@example.com', 'TQUMG8FMA5046Q3KH2G4PRX9I39XC7BG80409FMFY319Q4XCG73UC0U682V96FPJO'),
(4, 'Melba2', 'Abney88@nowhere.com', '6498XG72JH0R4Z807FJD008U93H2W5S8J4FF8T34510A6ZP57O8W49J'),
(5, 'Vanessa1985', 'Nicolette_Flores98@nowhere.com', '99OKD052510'),
(6, 'Wolf35', 'qjjvmdzc5602@example.com', '44T5D3U49974IA494JWVEDOI20VC199BARF3'),
(7, 'Gala2015', 'AldaTitus@example.com', '2LAIUZFIO4'),
(8, 'Buster45', 'Amaya@example.com', 'R99Z43C8X6J4KW5D2RD'),
(9, 'Aiko86', 'AustinClinton@example.com', '2QTJ40850985R12XML3FUY29P0E27B5D16IO858'),
(10, 'Abel614', 'crfipqej_kmjfuiub@example.com', 'YF6678552RN69S0H4W085KT125'),
(11, 'Alayna2019', 'Paquette664@nowhere.com', 'LANJ8T0QF8QIUWX9E8B0YVNM030I98F9EO9IA559512H72112O9CDO'),
(12, 'Kidd798', 'AlexanderTowns@example.com', 'C'),
(13, 'Gilliam1978', 'MalcomQBourgeois8@nowhere.com', 'R7UMW6S253S8L6893588EMZTQ8G0CS73UTQWNJ6T6PSHWOYY502IUHN45PP59UQ74VO8BS560056ZG3019B40MVV37P79487Z93146MT6VY88780B7Y554P'),
(14, 'Adam2004', 'Dion.Alger@example.com', '68FZ68GD96XY89111A0H7T7'),
(15, 'Allan7', 'Abigail.Isaacson@example.com', 'NP14OWRLE4HW8437NTK418J9'),
(16, 'Chadwick1985', 'ColeenAngulo327@example.com', 'N9'),
(17, 'Ronna2017', 'Gilson@example.com', 'S8MZ4J8'),
(18, 'Abernathy67', 'Gaither829@nowhere.com', '3PI7536353827'),
(19, 'Delma524', 'Harrell429@example.com', 'GM'),
(20, 'Abbott438', 'Felisha.Alonzo@example.com', '2L5HFK71D84JIX82R6G7EV5SFYTT7'),
(21, 'Abel1954', 'AuraCrooks@example.com', '38877Y'),
(22, 'Addie2008', 'Shanice_Bumgarner9@nowhere.com', 'E1'),
(23, 'Keiko2009', 'Lemmon996@example.com', 'KJ7SO039H01B7XLR1PUAI'),
(24, 'Moya1955', 'Collazo@example.com', '6034HK103J352KP4ACME06OF21HW9Q7NCMQHXL42QZB45S3TM78T6D1216B0B5X8X39O1V'),
(25, 'Arica2023', 'Roden@example.com', '3S266W'),
(26, 'Cameron372', 'Walton@nowhere.com', 'NAYD8673X96QT1SJ8WYTGC9N'),
(27, 'Digna489', 'Daren_Patten@example.com', '2YJ6XR6583R0X53EC0QX12O6KQ8AY8SI5XUC'),
(28, 'Kassie2', 'Sherley.Boehm663@example.com', 'I5DY6JE8UE'),
(29, 'Pacheco2001', 'Landon.Shephard1@nowhere.com', 'BUJB16P7433TR42RVAB9HM9G7835AA9A360S4E4B3F634IX7'),
(30, 'Emeline1963', 'ErnestineAleman@example.com', '7W8H52T3S8X9COM42OG1E7XN33ATAJN98227'),
(31, 'Crenshaw2022', 'DeneseMuniz4@example.com', '4MHF87P85414JM4I7X7W1NA938O726Q4U8SHKGVLKW6171N3W7JTNU1G8I0NUG777BH66344I3A6T0JX3D21STCH04XAQ4L4VR'),
(32, 'Isiah753', 'HaydenDallas@example.com', '03990GK05I111AG37E76LR99IB47ENE80WO8R7TN4H98Z8TCNG371'),
(33, 'Carlson148', 'Brianna.Mcgrath@nowhere.com', 'YS'),
(34, 'Kendall881', 'VirginiaRosen@example.com', 'BV314H6'),
(35, 'Nestor1953', 'NelidaAcosta@example.com', 'G4E5242ZK6Z46R0M2X2572J9ESEE1UN1YC19P70'),
(36, 'Abbie771', 'Ruff14@example.com', '54478JI2I2R9522LY8134K6GZ9GRYQT8OKD2WTMC2X0N49IBC8QR5267CKFEI438706BS7OE61P1I9H5VEA7B5ZM7KS0582V569VE82'),
(37, 'Amelia1964', 'HaroldLedesma@nowhere.com', '1D'),
(38, 'Izola457', 'Crisp18@nowhere.com', 'VHH'),
(39, 'Emmanuel1961', 'pdcw1967@example.com', 'ESSTG7NL744332H3528HPOUO6GPSHR0J52'),
(40, 'Royal678', 'Leavitt@nowhere.com', '2LA3LLJ8B4099X1R36W55D80RD74A'),
(41, 'Abrams1986', 'Barela997@example.com', 'VT8ZKJW5L17S10EB0744X300SPU3S8RS1221VQ43T33YFFK5U5T'),
(42, 'Mcnally2007', 'Adan.August@example.com', '418'),
(43, 'Cleopatra1', 'Ambrose_Adcock@example.com', '86U610DNMP36R4LS1QSPR09UDY0SZDA9HZ4YK21NAD5VBV44V70A97X1YZ64VTIVQE22'),
(44, 'Schaefer237', 'BuckAllman643@example.com', '1N'),
(45, 'Tyisha1988', 'Parrott49@example.com', 'H39E4AP3ASF679T7474'),
(46, 'Sanford125', 'odnhj3@nowhere.com', '73WJ2S07PH2030C04FG483YHOVK5220N3FYRL30155734GI0P652JTH2TI3GFW18'),
(47, 'Bonilla59', 'ElidaGreenfield53@example.com', 'FA7VB4X221RX3M7M84V4IT79'),
(48, 'Saucedo9', 'LawerenceYang5@example.com', '16O2DC8'),
(49, 'Doty81', 'Bert.Gibbs37@example.com', '3VX5K16FQD6NIO3'),
(50, 'Nathaniel1', 'WillisRoyster66@example.com', 'CIE0CO1D61W7Q976736E79');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `egresso`
--
ALTER TABLE `egresso`
  ADD UNIQUE KEY `cod_matricula` (`cod_matricula`),
  ADD KEY `cod_user` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `egresso`
--
ALTER TABLE `egresso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
