<nav class="sidebar col-xs-12 col-sm-4 col-lg-3 col-xl-2">
    <h1 class="site-title"><a href="../index.php"><em class="fa fa-home"></em> IFC</a></h1>
                                        
    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><em class="fa fa-bars"></em></a>
    <ul class="nav nav-pills flex-column sidebar-nav">
        <li class="nav-item"><a class="nav-link active" href="../index.php"><em class="fa fa-dashboard"></em> Home <span class="sr-only">(current)</span></a></li>
        <li class="nav-item"><a class="nav-link" href="lista_egressos.php"><em class="fa fa-calendar-o"></em> Todos os Egressos</a></li>
        <li class="nav-item"><a class="nav-link" href="lista_users.php"><em class="fa fa-calendar-o"></em> Todos os Users</a></li>
        <li class="nav-item"><a class="nav-link" href="charts.php"><em class="fa fa-bar-chart"></em> Charts</a></li>
       
        <li class="nav-item"><a class="nav-link" href="forms.php"><em class="fa fa-pencil-square-o"></em> Forms</a></li>
        
    </ul>
    <a href="logout.php" class="logout-button"><em class="fa fa-power-off"></em> Signout</a>
</nav>