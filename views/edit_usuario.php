<?php
	
	require_once '../models/dbconfig.php';
	require_once '../models/class.user.php';
	
	
	$update = new USER($DB_con);

	if(isset($_GET['edit_id']) && !empty($_GET['edit_id']))
	{
		$id = $_GET['edit_id'];
		$stmt_edit = $DB_con->prepare('SELECT user_name, user_email, user_pass FROM users WHERE user_id =:uid');
		$stmt_edit->execute(array(':uid'=>$id));
		$edit_row = $stmt_edit->fetch(PDO::FETCH_ASSOC);
	}
	
	
	if(isset($_POST['btn_save_updates']))
	{
		$novo_nome = $_POST['user_name'];// user name
		$novo_email = $_POST['user_email'];// user email
		$novo_senha = $_POST['user_pass'];// user pass
		
		if(!isset($errMSG))
		{
			$update -> update_user($novo_nome,$novo_email,$novo_senha,$id);

			if($update-> update_user($novo_nome,$novo_email,$novo_senha,$id)){
				?>
                <script>
				alert('Editado com sucesso ...');
				window.location.href='../index.php';
				</script>
                <?php
			}
			else{
				$errMSG = "Opa algo deu errado !";
			}
		
		}
	}

		if(isset($_POST['btn_delete'])){	
			
		

			$update -> delete_user($id);
			?>
                <script>
				alert('Deletado com sucesso ...');
				window.location.href='logout.php';
				</script>
			<?php	
		
		}					
	
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../images/favicon.ico">
    <title>Medialoot Bootstrap 4 Dashboard Template</title>

    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Icons -->
    <link href="../css/font-awesome.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/style.css" rel="stylesheet">

</head>
<body>
<div class="container-fluid" id="wrapper">
    <div class="row">
        <?php include "navbar.php"; ?>
        <main class="col-xs-12 col-sm-8 col-lg-9 col-xl-10 pt-3 pl-4 ml-auto">
            <header class="page-header row justify-center">
                <div class="col-md-6 col-lg-8" >
                    <h1 class="float-left text-center text-md-left">Charts</h1>
                </div>
                <?php include "../navuser.php"; ?>
					<div class="clear"></div>
				</header>
                <div class="col-sm-12 col-md-6">
								<div class="card mb-4">
									<div class="card-block">
										<h3 class="card-title">Contact Form</h3>
										<div class="dropdown card-title-btn-container">
											<button onclick="window.location.href='login.php'" name="voltar" class="btn btn-secondary btn-circle margin" type="button"><span class="fa fa-home"><a href="inicio.php"></a></span></button>
											</div>
										</div>
										
										<form class="form-horizontal" action="" method="post">
                                        <?php
                                            if(isset($errMSG)){
                                                ?>
                                                <div class="alert alert-danger">
                                                <span class="glyphicon glyphicon-info-sign"></span> &nbsp; <?php echo $errMSG; ?>
                                                </div>
                                                <?php
                                            }
                                        ?>
											<fieldset>
												<!-- Name input-->
												<div class="form-group">
													<label class="col-12 control-label no-padding" for="name">Seu nome</label>
													<div class="col-12 no-padding">
														<input type="text" placeholder="Your name" class="form-control" value="<?php echo $edit_row['user_name']; ?>"name="user_name" required />
													</div>
												</div>
											
												<!-- Email input-->
												<div class="form-group">
													<label class="col-12 control-label no-padding" for="email">Seu e-mail</label>
													<div class="col-12 no-padding">
														<input type="text" placeholder="Your email" class="form-control" value="<?php echo $edit_row['user_email']; ?>" name="user_email" required>
													</div>
												</div>
												
											<!-- password input-->
                                            <div class="form-group">
													<label class="col-12 control-label no-padding" for="email">Sua senha</label>
													<div class="col-12 no-padding">
														<input type="password" placeholder="Your password" class="form-control" value="<?php echo $edit_row['user_pass']; ?>" name="user_pass" required>
													</div>
												</div>
												
												<!-- Form actions -->
												<div class="form-group">
													<div class="col-12 widget-right no-padding">
														<button type="submit" name="btn_save_updates" class="btn btn-primary btn-md float-right">Atualizar</button>

                                                        <button type="submit" name="btn_delete" class="btn btn-m btn-danger">Deletar Conta</button>
													</div>
												</div>
											</fieldset>
										</form>
									</div>
								</div>
						</section>
					</div>
				</section>
			</main>
		</div>
	</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="../dist/js/bootstrap.min.js"></script>

<script src="../js/chart.min.js"></script>
<script src="../js/chart-data.js"></script>
<script src="../js/easypiechart.js"></script>
<script src="../js/easypiechart-data.js"></script>
<script src="../js/bootstrap-datepicker.js"></script>
<script src="../js/custom.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>

</body>
</html>
