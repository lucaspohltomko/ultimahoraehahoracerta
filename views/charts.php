<?php
include_once '../models/dbconfig.php';
include_once '../models/class.user.php';

if(!$user->is_loggedin())
{
 $user->redirect('login.php');
}

$user_id = $_SESSION['user_session'];

$stmt = $DB_con->prepare("SELECT * FROM egresso");
$stmt->execute();
$egressos=$stmt->fetchALL();



include_once("../chartphp/config.php"); 
include_once(CHARTPHP_LIB_PATH . "/inc/chartphp_dist.php"); 

$p = new chartphp(); 

// data array is populated from example data file 
include("data.php"); 
$p->data = $line_chart_data; 
$p->chart_type = "line"; 

// Common Options 
$p->title = "GRAFICO DE INGRESSOS POR ANO";
$p->xlabel = "ANOS";
$p->ylabel = "GAYS";
$p->series_label = "2014";

$linha = $p->render('c1'); 


$p = new chartphp(); 


// Common Options 
$p->title = "GRAFICO DE SATISFACAO"; 
// data array is populated from example data file 
$p->data = $pie_chart_data; 
$p->chart_type = "pie"; 

$pizza = $p->render('c1'); 

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="../images/favicon.ico">
	<script src="../chartphp/lib/js/jquery.min.js"></script> 
    <script src="../chartphp/lib/js/chartphp.js"></script> 
	<title>Medialoot Bootstrap 4 Dashboard Template</title>

    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Icons -->
    <link href="../css/font-awesome.css" rel="stylesheet">
    
    <!-- Custom styles for this template -->
    <link href="../css/style.css" rel="stylesheet">
    
</head>
<body>
	<div class="container-fluid" id="wrapper">
		<div class="row">
		<?php include "navbar.php"; ?>
			<main class="col-xs-12 col-sm-8 col-lg-9 col-xl-10 pt-3 pl-4 ml-auto">
				<header class="page-header row justify-center">
					<div class="col-md-6 col-lg-8" >
						<h1 class="float-left text-center text-md-left">GRAFICOS</h1>
					</div>
                    <?php include "../navuser.php"; ?>
					<div class="clear"></div>
				</header>
				<section class="row">
					<div class="col-sm-12">
						<?php echo $linha?> 
				
				<section class="row">
					<div class="col-sm-12">
						<?php echo $pizza ?> 
					</div>
						</section>

					</div>
				</section>
			</main>
		</div>
	</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="../https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="../dist/js/bootstrap.min.js"></script>
    
    <script src="../js/chart.min.js"></script>
    <script src="../js/chart-data.js"></script>
    <script src="../js/easypiechart.js"></script>
    <script src="../js/easypiechart-data.js"></script>
    <script src="../js/bootstrap-datepicker.js"></script>
    <script src="../js/custom.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    
	</body>
</html>
