<?php
include_once '../models/dbconfig.php';
include_once '../models/class.user.php';

if(!$user->is_loggedin())
{
 $user->redirect('login.php');
}
$user_id = $_SESSION['user_session'];


$usuario = $user->mostra_users();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../images/favicon.ico">
    <title>Lista Usuários</title>

    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Icons -->
    <link href="../css/font-awesome.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/style.css" rel="stylesheet">

</head>
<body>
<div class="container-fluid" id="wrapper">
    <div class="row">
        <?php include "navbar.php"; ?>
        <main class="col-xs-12 col-sm-8 col-lg-9 col-xl-10 pt-3 pl-4 ml-auto">
            <header class="page-header row justify-center">
                <div class="col-md-6 col-lg-8" >
                    <h1 class="float-left text-center text-md-left">Todos os Usuarios</h1>
                </div>
                <?php include "../navuser.php"; ?>
					<div class="clear"></div>
				</header>
                <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Pesquisar nomes..." title="Type in a name">
                <div class="table-content">
                <table class="table table-bordered table-responsive table-striped" id = "myTable">
                <thead class="table-dark">
                        <tr>
                        <th scope="col">id</th>
                        <th scope="col">nome</th>
                        <th scope="col">email</th>
                        <th scope="col">perfil</th>
                        <th scope="col">adm</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                        <?php 
                        $numero = 1;
                        foreach ($usuario as $usuarios) {?>
                            
                            <th scope="row"><?php echo $numero; $numero += 1;?></th>
                            
                                <td><?php echo $usuarios['user_name'];?></td>
                                <td><?php echo $usuarios['user_email'];?></td>
                                <td><button onclick="window.location.href=href='perfilEgresso.php?id=<?=$usuarios['user_id']?>'" class="btn btn-secondary btn-circle margin" type="button"><span class="fa fa-user"></span></button></td>
                                <?php if($_SESSION['admin']=="ativa"){?>
                                <td><button onclick="window.location.href=href='excluirUser.php?excluir=sim&id=<?=$usuarios['user_id']?>'" class="btn btn-secondary btn-circle margin" type="button"><span class="fa fa-gear"></span></button></td>
                                <?php } ?>
                            </tr>
                            <?php } ?>
                    </tbody>
                    </table>
                    </div>  


								
						</section>
					</div>
				</section>
			</main>
		</div>
	</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="../dist/js/bootstrap.min.js"></script>

<script src="../js/chart.min.js"></script>
<script src="../js/chart-data.js"></script>
<script src="../js/easypiechart.js"></script>
<script src="../js/easypiechart-data.js"></script>
<script src="../js/bootstrap-datepicker.js"></script>
<script src="../js/custom.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script>
function myFunction() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>
</body>
</html>
