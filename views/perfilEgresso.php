<?php
include_once '../models/dbconfig.php';
include_once '../models/class.user.php';

if(!$user->is_loggedin())
{
 $user->redirect('login.php');
}

$user_id = $_SESSION['user_session'];
$id = $_GET['id'];

$stmt = $DB_con->prepare("SELECT * FROM egresso WHERE id=:id");
$stmt->execute(array(":id"=>$id));
$egressoRow=$stmt->fetch(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../images/favicon.ico">
    <title>Medialoot Bootstrap 4 Dashboard Template</title>

    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Icons -->
    <link href="../css/font-awesome.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/style.css" rel="stylesheet">

</head>
<body>
<div class="container-fluid" id="wrapper">
    <div class="row">
        <?php include "navbar.php"; ?>
        <main class="col-xs-12 col-sm-8 col-lg-9 col-xl-10 pt-3 pl-4 ml-auto">
            <header class="page-header row justify-center">
                <div class="col-md-6 col-lg-8" >
                    <h1 class="float-left text-center text-md-left">Perfil de <?php echo $egressoRow['nome_completo'];?> </h1>
                </div>
                <?php include "../navuser.php"; ?>
                <div class="clear"></div>
                </header>
               
                
                                <div class="container">
                                <div class="clearfix"></div>

                                <form method="post" enctype="multipart/form-data" class="form-horizontal">


                                <?php
                                if(isset($errMSG)){
                                        ?>
                                <div class="alert alert-danger">
                                <span class="glyphicon glyphicon-info-sign"></span> &nbsp; <?php echo $errMSG; ?>
                                </div>
                                <?php
                                }
                                ?>


                                <table class="table table-bordered table-responsive table-striped">

                                <tr>
                                <td><label class="control-label">Nome</label></td>
                                <td><?php echo $egressoRow['nome_completo'];?></td>
                                </tr>

                                <tr>
                                <td><label class="control-label">Ano</label></td>

                                <td class><?php print($egressoRow['ano_ingresso']); ?></td>
                                </tr>

                                <tr>
                                <td><label class="control-label">Email</label></td>

                                <td class><?php print($egressoRow['ano_ingresso']); ?></td>
                                </tr>

                                <tr>
                                <td><label class="control-label">Curso</label></td>

                                <td class><?php print($egressoRow['curso']); ?></td>
                                </tr>
                                
                                <tr>
                                <td><label class="control-label">Nascimento</label></td>
                                <td><?php echo $egressoRow['nascimento'];?></td>
                                </tr>

                                <tr>
                                <td><label class="control-label">sexo</label></td>

                                <td class><?php print($egressoRow['sexo']); ?></td>
                                </tr>
                                
                                <tr>
                                <td><label class="control-label">Deficiencia</label></td>

                                <td class><?php print($egressoRow['deficiencia']); ?></td>
                                </tr>

                                <tr>
                                <td><label class="control-label">conc_curso</label></td>

                                <td class><?php print($egressoRow['conc_curso']); ?></td>
                                </tr>

                                <tr>
                                <td><label class="control-label">campus</label></td>

                                <td class><?php print($egressoRow['campus']); ?></td>
                                </tr>

                                <tr>
                                <td><label class="control-label">Área Formação</label></td>

                                <td class><?php print($egressoRow['area_formacao']); ?></td>
                                </tr>
                                
                                <tr>
                                <td><label class="control-label">Atuação</label></td>

                                <td class><?php print($egressoRow['atuacao']); ?></td>
                                </tr>

                                <tr>
                                <td><label class="control-label">trabalho</label></td>

                                <td class><?php print($egressoRow['trabalho']); ?></td>
                                </tr>

                                

                                </table>

                                </form>

                                </div>



              

								
						</section>
					</div>
				</section>
			</main>
		</div>
	</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="../dist/js/bootstrap.min.js"></script>

<script src="../js/chart.min.js"></script>
<script src="../js/chart-data.js"></script>
<script src="../js/easypiechart.js"></script>
<script src="../js/easypiechart-data.js"></script>
<script src="../js/bootstrap-datepicker.js"></script>
<script src="../js/custom.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>

</body>
</html>






