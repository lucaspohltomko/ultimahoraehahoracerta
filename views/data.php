<?php
include_once '../models/dbconfig.php';
include_once '../models/class.user.php';

if(!$user->is_loggedin())
{
 $user->redirect('login.php');
}

$user_id = $_SESSION['user_session'];

$stmt = $DB_con->prepare("SELECT * FROM egresso");
$stmt->execute();
$egressos=$stmt->fetchALL();

$_2010=0;
$_2011=0;
$_2012=0;
$_2013=0;
$_2014=0;
$_2015=0;
$_2016=0;
$_2017=0;
$_2018=0;



// contar egressos por ano


foreach($egressos as $x){
    if($x['ano_ingresso'] == "2010"){

        $_2010 += 1;
    }
}
foreach($egressos as $x){
    if($x['ano_ingresso'] == "2011"){
        $_2011 += 1;
    }
}
foreach($egressos as $x){
    if($x['ano_ingresso'] == "2012"){
        $_2012 += 1;
    }
}
foreach($egressos as $x){
    if($x['ano_ingresso'] == "2013"){
        $_2013 += 1;
    }
}
foreach($egressos as $x){
    if($x['ano_ingresso'] == "2014"){
        $_2014 += 1;
    }
}
foreach($egressos as $x){
    if($x['ano_ingresso'] == "2015"){
        $_2015 += 1;
    }
}
foreach($egressos as $x){
    if($x['ano_ingresso'] == "2016"){
        $_2016 += 1;
    }
}
foreach($egressos as $x){
    if($x['ano_ingresso'] == "2017"){
        $_2017 += 1;
    }
}
foreach($egressos as $x){
    if($x['ano_ingresso'] == "2018"){
        $_2018 += 1;
    }
}


//contar nota pelo total de notas

$pOtimo = 0;
$pMbom = 0;
$pBom = 0;
$pRegular = 0;
$pFraco = 0;
$nota = array();

foreach($egressos as $y){
    foreach($egressos as $x){
        // print_r($x['nota_ifc']);
        if($x['nota_ifc'] == "otimo"){
        // print_r($x);
            
            $pOtimo += 1;
            
        }
    }
    foreach($egressos as $x){
        if($x['nota_ifc']== "muito bom"){
            $pMbom += 1;
        }
    }
    foreach($egressos as $x){
        if($x['nota_ifc'] == "bom"){
            $pBom += 1;
        }
    }
    foreach($egressos as $x){
        if($x['nota_ifc'] == "regular"){
            $pRegular += 1;
        }
    }
    foreach($egressos as $x){
        if($x['nota_ifc'] == "fraco"){
            $pFraco += 1;
        }
    }
}

$totalDeVotos = $pOtimo + $pBom +$pFraco +$pMbom +$pRegular;
if($totalDeVotos != 0){
    $porotm = $pOtimo/$totalDeVotos;
    $pormbom = $pMbom/$totalDeVotos;
    $porbom = $pBom/$totalDeVotos;
    $porregular = $pRegular/$totalDeVotos;
    $porfraco = $pFraco/$totalDeVotos;
}
else{
    $porotm = 0;
    $pormbom = 0;
    $porbom = 0;
    $porregular = 0;
    $porfraco = 0;
}

//

$line_chart_data=array(
				array(
					array("2010",$_2010),
					array("2011",$_2011),
					array("2012",$_2012),
					array("2013",$_2013),
					array("2014",$_2014),
					array("2015",$_2015),
					array("2016",$_2016),
					array("2017",$_2017),
					array("2018",$_2018)
                    ));

$pie_chart_data=array(
                        array(
                            array('Otimo', $porotm),
                            array('Muito Bom', $pormbom),
                            array('Bom', $porbom),
                            array('regular', $porregular),
                            array('Fraco', $porfraco),
                            ));
                            
?>