<?php
include_once '../models/dbconfig.php';
include_once '../models/class.user.php';


if(!$user->is_loggedin())
{
    $user->redirect('login.php');
}
$user_id = $_SESSION['user_session'];
$stmt = $DB_con->prepare("SELECT * FROM users WHERE user_id=:user_id");
$stmt->execute(array(":user_id"=>$user_id));
$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
$id = $user_id;
echo $id;
if(isset($_GET['btn-signup']))
{

    $id = $user_id ;
    $nome = trim($_GET['nome_comp']);
    $cpf = trim($_GET['cpf']);
    $matricula = trim($_GET['matricula']);
    $ingresso = trim($_GET['ingresso']);
    $curso = trim($_GET['curso']);
    $cel = trim($_GET['cel']);
    $localizacao = trim($_GET['localizacao']);
    $endereco = trim($_GET['endereco']);
    $nascimento = trim($_GET['nascimento']);
    $sexo = trim($_GET['sexo']);
    $deficiencia = trim($_GET['deficiencia']);
    $conc_curso = trim($_GET['conc_curso']);
    $campus = trim($_GET['campus']);
    $nota_ifc = trim($_GET['nota_ifc']);
    $projeto = trim($_GET['projeto']);
    $area_formacao = trim($_GET['area_formacao']);
    $cursos_extra = trim($_GET['cursos_extra']);
    $palestra = trim($_GET['palestra']);
    $atuacao = trim($_GET['atuacao']);
    $trabalho = trim($_GET['trabalho']);
    $motivo_trabalho = trim($_GET['motivo_trabalho']);
    $salario = trim($_GET['salario']);
    $texto = trim($_GET['texto']);


    try
    {
        if($user->registrar_egresso($id,$nome,$cpf,$matricula,$ingresso,$curso,$cel,$localizacao,$endereco,$nascimento,$sexo,$deficiencia,$conc_curso,$campus,$nota_ifc,$projeto,$area_formacao,$cursos_extra,$palestra,$atuacao,$trabalho,$motivo_trabalho,$salario,$texto))
        {
            $user->redirect('inicio.php');
        }
    }
    catch(PDOException $e){
        echo $e->getMessage();
    }





}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../images/favicon.ico">
    <title>Formulario Egresso</title>

    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Icons -->
    <link href="../css/font-awesome.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/style.css" rel="stylesheet">

</head>
<body>
<div class="container-fluid" id="wrapper">
    <div class="row">
        <?php include "navbar.php"; ?>
        <main class="col-xs-12 col-sm-8 col-lg-9 col-xl-10 pt-3 pl-4 ml-auto">
            <header class="page-header row justify-center">
                <div class="col-md-6 col-lg-8" >
                    <h1 class="float-left text-center text-md-left">Formulasso</h1>
                </div>
                <?php include "../navuser.php"; ?>
					<div class="clear"></div>
				</header>
				<section class="row">
					<div class="col-sm-12">
						<section class="row">
							<div class="col-12">
								<div class="card mb-4">
									<div class="card-block">
									
										<form class="form" action="#">
                                            <form method="post">
                                                <div class="form-group row">
                                                    <div class="col-md-9">
                                                        <label class="col-md-3 col-form-label" for="nome"></label>
                                                        <h4>Nome Completo</h4>
                                                    <input type="text" class="form-control" name="nome_comp" placeholder="ex: João Almeida Morais da Silva">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-9">
                                                    <label class="col-md-3 col-form-label" for="cpf"></label>
                                                        <h4>Cpf</h4>
                                                    <input type="number" class="form-control" name="cpf" placeholder="ex:xxxxxxxxxxx">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-9">
                                                    <label class="col-md-3 col-form-label" for="matricula"></label>
                                                        <h4>Matricula</h4>
                                                    <input type="number" class="form-control" name="matricula" placeholder="ex:2016123456">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-9">
                                                    <label class="col-md-3 col-form-label" for="ingresso"></label>
                                                        <h4>Ingresso</h4>
                                                    <input type="number" min="-9999" max="9999" class="form-control" name="ingresso" placeholder="ex: 2016">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-9">
                                                    <label class="col-md-3 col-form-label" for="curso"></label>
                                                        <h4>Curso</h4>
                                                        <div class="radio">
                                                            <h6><input type="radio" name="curso" value="TAS">Técnico em Agrimensura Subsequente ao Ensino Médio.</h6>
                                                            <h6><input type="radio" name="curso" value="TAI">Técnico em Agropecuária Integrado ao Ensino Médio</h6>
                                                            <h6><input type="radio" name="curso" value="TII">Técnico em Informática Integrado ao Ensino Médio</h6>
                                                            <h6><input type="radio" name="curso" value="TQI">Técnico em Química Integrado ao Ensino Médio</h6>
                                                            <h6><input type="radio" name="curso" value="BA">Bacharelado em Agronomia.</h6>
                                                            <h6><input type="radio" name="curso" value="BMV">Bacharelado em Medicina Veterinária.</h6>
                                                            <h6><input type="radio" name="curso" value="BSI">Bacharelado em Sistemas de Informação.</h6>
                                                            <h6><input type="radio" name="curso" value="LCA">Licenciatura em Ciências Agrícolas.</h6>
                                                            <h6><input type="radio" name="curso" value="LQ">Licenciatura em Química.</h6>
                                                            <h6><input type="radio" name="curso" value="TRC">Tecnologia em Redes de Computadores.</h6>
                                                            <h6><input type="radio" name="curso" value="EA">Especialização em Aquicultura.</h6>
                                                            <h6><input type="radio" name="curso" value="EEM"> Especialização em Educação Matemática.</h6>
                                                            <h6><input type="radio" name="curso" value="MPSA">Mestrado em Produção e Sanidade Animal.</h6>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-9">
                                                    <label class="col-md-3 col-form-label" for="cel"></label>
                                                        <h4>Celular</h4>
                                                    <input type="number" class="form-control" name="cel" placeholder="ex: 479996241328">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-9">
                                                    <label class="col-md-3 col-form-label" for="localizacao"></label>
                                                        <h4>Cidade e Estado em que reside:</h4>
                                                    <input type="text" class="form-control" name="localizacao" placeholder="ex: Joinville/SC">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-9">
                                                    <label class="col-md-3 col-form-label" for="endereco"></label>
                                                        <h4>Endereço completo:</h4>
                                                    <input type="text" class="form-control" name="endereco" placeholder="ex: Rua da Vida, 1000, Jardim da Saudade, casa, 9999-000">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-9">
                                                    <label class="col-md-3 col-form-label" for="nascimento"></label>
                                                        <h4>Data de nascimento:</h4>
                                                    <input type="date" class="form-control" name="nascimento" placeholder="">
                                                    </div>
                                                </div>


                                                <label class="col-md-3 col-form-label" ></label>
                                                <h4>Se identifica com que gênero</h4>
                                                <div class="col-md-9">
                                                <div class="radio">
                                                    <h6><input type="radio" name="sexo" value="Feminino">Feminino</h6>
                                                    <h6><input type="radio" name="sexo" value="Masculino">Masculino</h6>
                                                    <h6><input type="radio" name="sexo" value="Prefiro não dizer">Prefiro não dizer</h6>
                                                </div>
                                                </div>

                                                <label class="col-md-3 col-form-label" ></label>
                                                <h4>Você tem algum tipo de deficiencia?</h4>
                                                <div class="col-md-9">
                                                <div class="radio">
                                                    <h6><input type="radio" name="deficiencia" value="Não Possui">Não.</h6>
                                                    <h6><input type="radio" name="deficiencia" value="Motora">Sim, motora.</h6>
                                                    <h6><input type="radio" name="deficiencia" value="Visual">Sim, visual.</h6>
                                                    <h6><input type="radio" name="deficiencia" value="Auditiva">Sim, auditiva.</h6>
                                                    <h6><input type="radio" name="deficiencia" value="Cognitiva">Sim, cognitiva.</h6>
                                                    <h6><input type="radio" name="deficiencia" value="Prefiro não dizer">Prefiro não dizer.</h6>
                                                </div>
                                                </div>

                                                <label class="col-md-3 col-form-label" ></label>
                                                <h4>Você já concluiu um ou mais cursos no IFC?<br>Marcar a(s) opção(ões) que lhe dizem respeito o ano ingressado e de conclusão.</h4>
                                                <div class="col-md-9">
                                                <div class="radio">
                                                    <h6><input type="radio" name="conc_curso" value="Ensino Médio Integrado">Ensino Médio Integrado.</h6>
                                                    <h6><input type="radio" name="conc_curso" value="Ensino Médio Subsequente">Ensino Médio Subsequente.</h6>
                                                    <h6><input type="radio" name="conc_curso" value="Ensino Médio Concomitante">Ensino Médio Concomitante.</h6>
                                                    <h6><input type="radio" name="conc_curso" value="Graduação - Bacharelado">Graduação - Bacharelado.</h6>
                                                    <h6><input type="radio" name="conc_curso" value="Graduação - Licenciatura">Graduação - Licenciatura.</h6>
                                                    <h6><input type="radio" name="conc_curso" value="Graduação - Tecnólogo">Graduação - Tecnólogo.</h6>
                                                    <h6><input type="radio" name="conc_curso" value="Pós-graduação lato sensu (especialização)">Pós-graduação lato sensu (especialização).</h6>
                                                    <h6><input type="radio" name="conc_curso" value="Pós-graduação stricto sensu (mestrado)">Pós-graduação stricto sensu (mestrado).</h6>
                                                    <h6><input type="radio" name="conc_curso" value="Não, me formei em outra instituição">Não, me formei em outra instituição.</h6>
                                                </div>
                                                </div>

                                                <label class="col-md-3 col-form-label" ></label>
                                                <h4>Caso você já tenha se formado em algum curso no IFC, selecione o campus:<br>(tenha como base o último curso em que você se formou/concluiu, no IFC)</h4>
                                                <div class="col-md-9">
                                                <div class="radio">
                                                    <h6><input type="radio" name="campus" value="Alberado">Abelardo Luz.</h6>
                                                    <h6><input type="radio" name="campus" value="Araquari">Araquari.</h6>
                                                    <h6><input type="radio" name="campus" value="Blumenau">Blumenau.</h6>
                                                    <h6><input type="radio" name="campus" value="Brusque">Brusque.</h6>
                                                    <h6><input type="radio" name="campus" value="Camburiu">Camboriú.</h6>
                                                    <h6><input type="radio" name="campus" value="Concordia">Concórdia.</h6>
                                                    <h6><input type="radio" name="campus" value="Fraiburgo">Fraiburgo.</h6>
                                                    <h6><input type="radio" name="campus" value="Ibirama">Ibirama.</h6>
                                                    <h6><input type="radio" name="campus" value="Ruzerna">Luzerna.</h6>
                                                    <h6><input type="radio" name="campus" value="Rio do Sul">Rio do Sul.</h6>
                                                    <h6><input type="radio" name="campus" value="Santa Rosa">Santa Rosa do Sul.</h6>
                                                    <h6><input type="radio" name="campus" value="Sao Bento">São Bento do Sul.</h6>
                                                    <h6><input type="radio" name="campus" value="Sao Francisco">São Francisco do Sul.</h6>
                                                    <h6><input type="radio" name="campus" value="Sombrio">Sombrio.</h6>
                                                    <h6><input type="radio" name="campus" value="Videira">Videira.</h6>
                                                    <h6><input type="radio" name="campus" value="Nenhum campus do IFC">O último curso que conclui/me formei não foi em nenhum dos campi do IFC.</h6>
                                                </div>
                                                </div>

                                                <label class="col-md-3 col-form-label" ></label>
                                                <h4>Qual conceito você atribui ao IFC, como instituição?</h4>
                                                <div class="col-md-9">
                                                <div class="radio">
                                                    <h6><input type="radio" name="nota_ifc" value="otimo">Ótimo</h6>
                                                    <h6><input type="radio" name="nota_ifc" value="muito bom">Muito bom.</h6>
                                                    <h6><input type="radio" name="nota_ifc" value="bom">Bom.</h6>
                                                    <h6><input type="radio" name="nota_ifc" value="regular">Regular.</h6>
                                                    <h6><input type="radio" name="nota_ifc" value="fraco">Fraco.</h6>
                                                </div>
                                                </div>

                                                <label class="col-md-3 col-form-label" ></label>
                                                <h4>Você participou de algum projeto de pesquisa ou extensão?</h4>
                                                <div class="col-md-9">
                                                <div class="radio">
                                                    <h6><input type="radio" name="projeto" value="Sim, voluntario">Sim, como voluntário.</h6>
                                                    <h6><input type="radio" name="projeto" value="Sim, bolsista">Sim, como bolsista.</h6>
                                                    <h6><input type="radio" name="projeto" value="Não">Não</h6>
                                                </div>
                                                </div>

                                                <label class="col-md-3 col-form-label" ></label>
                                                <h4>Seus interesses na continuidade dos estudos são:</h4>
                                                <div class="col-md-9">
                                                <div class="radio">
                                                    <h6><input type="radio" name="area_formacao" value="Mesma área da última formação">Na mesma área da minha útlima formação.</h6>
                                                    <h6><input type="radio" name="area_formacao" value="Área diferente da útima formação">Em área diferente da minha última formação.</h6>
                                                    <h6><input type="radio" name="area_formacao" value="Não tenho interesse em continuar estudando">Não tenho interesse em continuar estudando.</h6>
                                                </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-9">
                                                    <label class="col-md-3 col-form-label" ></label>
                                                        <h4>Caso você tenha interesse em regressar ou continuar os estudos no IFC, descreva, o(s) curso(s) e o campus (localidade) que você gostaria de fazer.</h4>
                                                    <input type="text" class="form-control" name="cursos_extra" placeholder="ex: Técnico Integrado em Eletrotécnica - Blumenau">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-9">
                                                    <label class="col-md-3 col-form-label" for="palestra"></label>
                                                        <h4>Você teria interesse em ministrar, gratuitamente, curso ou palestra no IFC? Em caso afirmativo, em qual área e qual localidade?</h4>
                                                    <input type="text" class="form-control" name="palestra" placeholder="ex: Palestra sobre integração social, na região de Araquari">
                                                    </div>
                                                </div>

                                                <div class="radio">
                                                    <div class="col-md-9">
                                                    <label class="col-md-3 col-form-label" for="atuacao"></label>
                                                    <h4>Atualmente você:<br>tenha como base o último curso em que você se formou/concluiu, no IFC</h4>

                                                    <h6><input type="radio" name="atuacao" value="Não está atuando profissionalmente">Não está atuando profissionalmente.</h6>
                                                    <h6><input type="radio" name="atuacao" value="Está atuando na mesma área de formação do curso técnico">Está atuando na mesma área de formação do curso técnico (concomitante, integrado, subsequente).</h6>
                                                    <h6><input type="radio" name="atuacao" value="Está atuando em área diferente da formação do curso técnico">Está atuando em área diferente da formação do curso técnico (concomitante, integrado, subsequente).</h6>
                                                    <h6><input type="radio" name="atuacao" value="Está atuando na mesma área de formação do curso de graduação">Está atuando na mesma área de formação do curso de graduação (bacharelado, licenciatura, tecnólogo).</h6>
                                                    <h6><input type="radio" name="atuacao" value="Está atuando em área diferente da formação do curso de graduação">Está atuando em área diferente da formação do curso de graduação (bacharelado, licenciatura, tecnólogo).</h6>
                                                    <h6><input type="radio" name="atuacao" value="Está atuando na mesma área de formação do curso de pós-graduação">Está atuando na mesma área de formação do curso de pós-graduação (especialização).</h6>
                                                    <h6><input type="radio" name="atuacao" value="Está atuando em área diferente de formação do curso de pós-graduação">Está atuando em área diferente de formação do curso de pós-graduação (especialização).</h6>
                                                    <h6><input type="radio" name="atuacao" value="Está atuando na mesma área de formação do curso de pós-graduação">Está atuando na mesma área de formação do curso de pós-graduação (mestrado).</h6>
                                                    <h6><input type="radio" name="atuacao" value="Está atuando em área diferente da formação do curso de pós-graduação">Está atuando em área diferente da formação do curso de pós-graduação (mestrado).</h6>
                                                </div>
                                                </div>

                                                <label class="col-md-3 col-form-label" ></label>
                                                <h4>Situação de trabalho:</h4>
                                                <div class="col-md-9">
                                                <div class="radio">
                                                    <h6><input type="radio" name="trabalho" value="Empregado com carteira assinada">Empregado com carteira assinada.</h6>
                                                    <h6><input type="radio" name="trabalho" value="Funcionário público">Funcionário público.</h6>
                                                    <h6><input type="radio" name="trabalho" value="Estágio remunerado">Estágio remunerado.</h6>
                                                    <h6><input type="radio" name="trabalho" value="Estágio não remunerado">Estágio não remunerado.</h6>
                                                    <h6><input type="radio" name="trabalho" value="Autônomo/prestador de serviços/trabalha por conta própria">Autônomo/prestador de serviços/trabalha por conta própria.</h6>
                                                    <h6><input type="radio" name="trabalho" value="Não se aplica">Não se aplica (caso você não esteja atuando profissionalmente).</h6>
                                                </div>
                                                </div>

                                                <label class="col-md-3 col-form-label" ></label>
                                                <h4>Qual o motivo de não estar trabalhando na área de formação?<br>De acordo com a resposta anterior</h4>
                                                <div class="col-md-9">
                                                <div class="radio">
                                                    <h6><input type="radio" name="motivo_trabalho" value="Falta vaga">Por falta de vagas na área.</h6>
                                                    <h6><input type="radio" name="motivo_trabalho" value="Baixo salario">Por baixos salários.</h6>
                                                    <h6><input type="radio" name="motivo_trabalho" value="Escolha vocacional equivocada">Por escolha vocacional equivocada.</h6>
                                                    <h6><input type="radio" name="motivo_trabalho" value="Estou estudando">Por estar estudando.</h6>
                                                    <h6><input type="radio" name="motivo_trabalho" value="Por escolha">Por escolha.</h6>
                                                </div>
                                                </div>

                                                <label class="col-md-3 col-form-label" ></label>
                                                <h4>Na sua opinião, em que faixa se enquadra sua remuneração?<br>Marque a opção que mais se aproxima.</h4>
                                                <div class="col-md-9">
                                                <div class="radio">
                                                    <h6><input type="radio" name="salario" value="Até 1 salário mínimo">Até 1 salário mínimo.</h6>
                                                    <h6><input type="radio" name="salario" value="De 1 a 2 salários mínimos">De 1 a 2 salários mínimos.</h6>
                                                    <h6><input type="radio" name="salario" value="De 2 a 3 salários mínimos">De 2 a 3 salários mínimos.</h6>
                                                    <h6><input type="radio" name="salario" value="De 3 a 4 salários mínimos">De 3 a 4 salários mínimos.</h6>
                                                    <h6><input type="radio" name="salario" value="De 4 a 5 salários mínimos">De 4 a 5 salários mínimos.</h6>
                                                    <h6><input type="radio" name="salario" value="De 5 a 10 salários mínimos">De 5 a 10 salários mínimos.</h6>
                                                    <h6><input type="radio" name="salario" value="Acima de 10 salários mínimos">Acima de 10 salários mínimos.</h6>
                                                    <h6><input type="radio" name="salario" value="Não se aplica, profissional">Não se aplica (caso você não esteja atuando profissionalmente).</h6>
                                                    <h6><input type="radio" name="salario" value="Não se aplica, voluntario">Não se aplica (caso esteja fazendo trabalho voluntário).</h6>
                                                    <h6><input type="radio" name="salario" value="Não se aplica, estágio não remunerado">Não se aplica (caso esteja em estágio não remunerado).</h6>
                                                    <h6><input type="radio" name="salario" value="nDizer">Prefiro não dizer.</h6>

                                                </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-9">
                                                        <label class="col-md-3 col-form-label" for="comment"></label>
                                                        <h4>Utilize este espaço para deixar uma mensagem para nós.</h4>
                                                    <textarea class="form-control" rows="" name="texto"></textarea>
                                                    </div>
                                                </div>






                                                <div class="form-group row">
                                                    <button type="submit" class="btn btn-block btn-primary" name="btn-signup">
                                                        CADASTRAR
                                                    </button>
                                                </div>

				</section>
			</main>
		</div>
	</div>

	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="../dist/js/bootstrap.min.js"></script>

<script src="../js/chart.min.js"></script>
<script src="../js/chart-data.js"></script>
<script src="../js/easypiechart.js"></script>
<script src="../js/easypiechart-data.js"></script>
<script src="../js/bootstrap-datepicker.js"></script>
<script src="../js/custom.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>

</body>
</html>
